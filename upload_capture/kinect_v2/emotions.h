// emotions.h - Emotion categories/names.
// Copyright (C) 2011  Rob Myers rob@robmyers.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or 
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

extern const unsigned int NUM_EMOTIONS;
extern const char * EMOTIONS[];

// How many seconds of data to capture each emotion for
extern const unsigned int SECONDS_TO_CAPTURE_EMOTION_FOR;

// The delay before reading emotion data
extern const unsigned int SECONDS_TO_WAIT_BEFORE_CAPTURING;
